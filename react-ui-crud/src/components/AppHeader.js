import React, { Component } from 'react';
import {
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Collapse,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,Button
} from 'reactstrap';

class AppHeader extends Component {

    state = {
        isOpen: false
    };
    toggle = this.toggle.bind(this);

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    logout=()=>{
        console.log('logging out');
        localStorage.removeItem('currentUser');
        this.props.history.push({pathname:'/login'})
    }

    render() {

        
    let logoutButton = localStorage.getItem('currentUser') != undefined?  <Button className="pullRight" onClick={this.logout}>Logout</Button> : null;

        return <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/">
                <img src="https://cdn-alnbj.nitrocdn.com/UABFRiSYcuFNUizeGciWowjnvkBpwQwl/assets/static/source/rev-0eb185a/wp-content/uploads/2020/04/Logo-kambda-white.svg" width="128" className="d-inline-block align-top" alt="" />
            </NavbarBrand>
            {logoutButton}
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                </Nav>
            </Collapse>
        </Navbar>;
    }
}

export default AppHeader;