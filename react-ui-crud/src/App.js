import React, { Component, Fragment } from 'react';
import AppHeader from './components/AppHeader';
import Home from './components/Home';
import AppFooter from './components/AppFooter';
import { BrowserRouter,Route } from 'react-router-dom'
import { LoginPage} from  './containers/LoginPage'
class App extends Component {

  render() {
    return    <BrowserRouter><Fragment>
      <AppHeader />

      <Route path="/" exact component={Home} />
      <Route path="/login/" exact component={LoginPage} />
    
      <AppFooter />
    </Fragment>;
    </BrowserRouter>
  }
}

export default App;
