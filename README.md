# Kambda ReactASPNET Demo Task
- Add basic users authentication
- Add JSON Web token header to the api endpoints
- Modify the user service to use a database (feel free to use the one you feel fits better)

**- Modify the user service to use a database (feel free to use the one you feel fits better)**

I developed and included a full backend solution to support a database, in a full decoupled scenario.  The WebApi is agnostic from the Application Core and the Database   through an interface , to ensure the even the API is fully decoupled from all the other components. 

Backend.Application- Class Library 

Contains all the application business logic. Uses the Command and Query Responsibility Segregation (CQRS) Pattern to manage all the operations supported.  

Backend.Common – Class Library 

 Contains common enumerations and values that are used across all the app. Like environment settings and/or configurations.  

Backend.Domain – Class Library 

Contains the modeling of all Database Objects in a POCO format. Can be reused to work with another RDBMS. The configuration for the POCO properties is built with a fluent configuration with Entity Framework Core.  

Backend.Infraestructure -Class Library 

Contains the infrastructure configuration like available databases, services and definition of entities in the RDBMS. This module configures the domain validation with a fluent language. Is independent from the Database Vendor and can be reused to support other vendors ( SQLite, Postgre, SQL Server , etc )


**- Add JSON Web token header to the api endpoints**

In this case I used an Entity Framework Identity Provider to configure and manage users, claims, etc.  I'm used to implement it using Identity Server 4 so the app is able to authenticate with an external provider (Custom, Google, etc) but that would have taken too long , so I implemented the JWT token funtionality within the application. 

I used this article as a reference
 https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/
 
 and modified some components to support dependency injection to the AutenticationContext. 


**Add basic users authentication**

In the react app, I managed to connect the app to the authService and store the Token in Local Storage.  If the user is not authenticated, then it wont be able to visit the users page. 

Note : The only protected route from the WebApi is the /users/get route. I think this should suffice for the requirement of consuming a protected api, even if the other routes are not.


Included in the WebApi solution, you will find a POSTMAN collection under /Postman folder. Feel free to use it to test the API.

Thank you!

