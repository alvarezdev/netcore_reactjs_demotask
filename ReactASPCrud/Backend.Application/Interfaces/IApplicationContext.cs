﻿
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Backend.Domain;
namespace Backend.Application.Interfaces
{
    public interface IApplicationDbContext
    {

         DbSet<User> User { get; set; }
         Task<int> SaveChangesAsync(CancellationToken cancellationToken= new CancellationToken());
    }
}
