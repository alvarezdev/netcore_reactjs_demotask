﻿using Backend.Application.Interfaces;
using Backend.Domain;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Backend.Application.UserLogic
{
    public class UserCreate_RQ : IRequest<Backend.Domain.User>
    {
        public User User { get; set; }
    }

    public class UserCreate_Handler : IRequestHandler<UserCreate_RQ, Backend.Domain.User>
    {
        public IApplicationDbContext _context;
        public UserCreate_Handler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Domain.User> Handle(UserCreate_RQ request, CancellationToken cancellationToken)
        {
            _context.User.Add(request.User);
            await _context.SaveChangesAsync();
            return request.User;
        }


    }



    public class UserUpdate_RQ : IRequest<bool>
    {
        public User User { get; set; }
    }
    public class UserUpdate_Handler : IRequestHandler<UserUpdate_RQ, bool>
    {
        public IApplicationDbContext _context;
        public UserUpdate_Handler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<bool> Handle(UserUpdate_RQ request, CancellationToken cancellationToken)
        {
            var user = _context.User.SingleOrDefault(x => x.Id == request.User.Id);

            user.Name = request.User.Name;
            user.Document = request.User.Document;
            user.Email = request.User.Email;
            user.Phone = request.User.Phone;


           
            var result = await _context.SaveChangesAsync();
            return result != 0;
        }


    }


    public class UserDelete_RQ : IRequest<bool>
    {
        public int UserId { get; set; }
    }

    public class UserDelete_Handler : IRequestHandler<UserDelete_RQ, bool>
    {
        public IApplicationDbContext _context;
        public UserDelete_Handler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<bool> Handle(UserDelete_RQ request, CancellationToken cancellationToken)
        {
            var user  = _context.User.SingleOrDefault(x => x.Id == request.UserId);
            if (user == null) return false; //not found.
            
            _context.User.Remove(user);
            var result =  await _context.SaveChangesAsync();
            return result != 0;
        }

     
    }



}
