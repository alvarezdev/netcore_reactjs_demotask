﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

using ReactASPCrud.Services;

namespace ReactASPCrud.Controllers
{
  //  [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("ReactPolicy")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        // GET api/users
        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {

            return await userService.GetAll();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await userService.GetById(id));
        }

        // POST api/users
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] User user)
        {
            var newUser = await userService.Create(user);
            return CreatedAtAction("Get", new { id = newUser.Id }, newUser);
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] User user)
        {
           

            if (user.Id == 0) user.Id = id;

            var newUser = await userService.Update(user);


            return NoContent();
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
         

           await  userService.Delete(id);

            return NoContent();
        }

        public override NoContentResult NoContent()
        {
            return base.NoContent();
        }
    }
}
