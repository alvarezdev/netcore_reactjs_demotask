﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Domain.Entities.Identity
{


    public class IdentityResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
