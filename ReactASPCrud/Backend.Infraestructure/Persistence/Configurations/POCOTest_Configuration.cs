﻿
using Backend.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.Infraestructure.Persistence.Configurations
{
   public class User_Configuration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(k => k.Id).Metadata.IsPrimaryKey();
            builder.Property(e => e.Id).ValueGeneratedOnAdd();
            builder.ToTable("User");
        }
    }
}
