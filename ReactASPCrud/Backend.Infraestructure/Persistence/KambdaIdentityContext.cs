﻿
using Application.Common.Interfaces;
using Backend.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

#nullable disable

namespace Backend.Infraestructure
{
    public partial class KambdaIdentityContext : IdentityDbContext<ApplicationUser>, IIdentityContext
    {
        public KambdaIdentityContext() : base()
        {

        }
        public KambdaIdentityContext(DbContextOptions<KambdaIdentityContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder

                    .EnableSensitiveDataLogging(false)
                    .UseSqlite("Filename=KambdaTest.db",
                    b =>
                    {
                        b.MigrationsAssembly(typeof(KambdaIdentityContext).Assembly.FullName);
                    });
            }



            base.OnConfiguring(optionsBuilder);
        }

    }
}
