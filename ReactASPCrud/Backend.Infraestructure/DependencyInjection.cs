﻿
using Application.Common.Interfaces;
using Backend.Application.Interfaces;
using Backend.Identity;
using Backend.Infraestructure.Persistence;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Backend.Infraestructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {


            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(
                    /*configuration.GetConnectionString("AzureDb")*/
                    "Filename=KambdaTest.db",
                    b => { 
                        b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName);
                    }));


            services.AddDbContext<KambdaIdentityContext>(options =>
                options.UseSqlite(
                    /*configuration.GetConnectionString("AzureDb")*/
                    "Filename=KambdaTest.db",
                    b => {
                        b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName);
                    }));
           

            services.AddIdentity<ApplicationUser, IdentityRole>()
               .AddEntityFrameworkStores<KambdaIdentityContext>()
               .AddDefaultTokenProviders();

          

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            services.AddScoped<IIdentityContext>(provider => provider.GetService<KambdaIdentityContext>());
            //services.AddTransient<IDateTime, DateTimeService>();

            return services;
        }

    }
}
